import React from "react";
import { Row } from "antd";
import "./page.css";

const ShipmentManagement = () => {
  return (
    <Row justify="center" style={{ marginTop: "10%" }}>
      <h1 style={{ fontSize: "58px", color: "#0540f2" }}>
        Under
        <br />
        Development
      </h1>
    </Row>
  );
};

export default ShipmentManagement;
